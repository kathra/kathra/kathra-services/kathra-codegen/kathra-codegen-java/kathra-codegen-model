package org.kathra.codegen.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* CodeGenTemplateArgument
*/
@ApiModel(description = "CodeGenTemplateArgument")
public class CodeGenTemplateArgument {

    protected String key = null;
    protected String value = null;
    protected String contrainst = null;


    /**
    * Quick Set key
    * Useful setter to use in builder style (eg. myCodeGenTemplateArgument.key(String).anotherQuickSetter(..))
    * @param String key
    * @return CodeGenTemplateArgument The modified CodeGenTemplateArgument object
    **/
    public CodeGenTemplateArgument key(String key) {
        this.key = key;
        return this;
    }

    /**
    * Argument key to generate codegen
    * @return String key
    **/
    @ApiModelProperty(value = "Argument key to generate codegen")
    public String getKey() {
        return key;
    }

    /**
    * Argument key to generate codegen
    **/
    public void setKey(String key) {
        this.key = key;
    }

    /**
    * Quick Set value
    * Useful setter to use in builder style (eg. myCodeGenTemplateArgument.value(String).anotherQuickSetter(..))
    * @param String value
    * @return CodeGenTemplateArgument The modified CodeGenTemplateArgument object
    **/
    public CodeGenTemplateArgument value(String value) {
        this.value = value;
        return this;
    }

    /**
    * Argument value to generate codegen
    * @return String value
    **/
    @ApiModelProperty(value = "Argument value to generate codegen")
    public String getValue() {
        return value;
    }

    /**
    * Argument value to generate codegen
    **/
    public void setValue(String value) {
        this.value = value;
    }

    /**
    * Quick Set contrainst
    * Useful setter to use in builder style (eg. myCodeGenTemplateArgument.contrainst(String).anotherQuickSetter(..))
    * @param String contrainst
    * @return CodeGenTemplateArgument The modified CodeGenTemplateArgument object
    **/
    public CodeGenTemplateArgument contrainst(String contrainst) {
        this.contrainst = contrainst;
        return this;
    }

    /**
    * Argument constraint
    * @return String contrainst
    **/
    @ApiModelProperty(value = "Argument constraint")
    public String getContrainst() {
        return contrainst;
    }

    /**
    * Argument constraint
    **/
    public void setContrainst(String contrainst) {
        this.contrainst = contrainst;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CodeGenTemplateArgument codeGenTemplateArgument = (CodeGenTemplateArgument) o;
        return Objects.equals(this.key, codeGenTemplateArgument.key) &&
        Objects.equals(this.value, codeGenTemplateArgument.value) &&
        Objects.equals(this.contrainst, codeGenTemplateArgument.contrainst);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value, contrainst);
    }
}

