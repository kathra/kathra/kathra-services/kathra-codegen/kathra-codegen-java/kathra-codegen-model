package org.kathra.codegen.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
* CodeGenTemplate
*/
@ApiModel(description = "CodeGenTemplate")
public class CodeGenTemplate {

    protected String name = null;
    protected List<CodeGenTemplateArgument> arguments = null;


    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myCodeGenTemplate.name(String).anotherQuickSetter(..))
    * @param String name
    * @return CodeGenTemplate The modified CodeGenTemplate object
    **/
    public CodeGenTemplate name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Template Name
    * @return String name
    **/
    @ApiModelProperty(value = "Template Name")
    public String getName() {
        return name;
    }

    /**
    * Template Name
    **/
    public void setName(String name) {
        this.name = name;
    }

    /**
    * Quick Set arguments
    * Useful setter to use in builder style (eg. myCodeGenTemplate.arguments(List<CodeGenTemplateArgument>).anotherQuickSetter(..))
    * @param List<CodeGenTemplateArgument> arguments
    * @return CodeGenTemplate The modified CodeGenTemplate object
    **/
    public CodeGenTemplate arguments(List<CodeGenTemplateArgument> arguments) {
        this.arguments = arguments;
        return this;
    }

    public CodeGenTemplate addArgumentsItem(CodeGenTemplateArgument argumentsItem) {
        if (this.arguments == null) {
            this.arguments = new ArrayList();
        }
        this.arguments.add(argumentsItem);
        return this;
    }

    /**
    * CodeGen Argument
    * @return List<CodeGenTemplateArgument> arguments
    **/
    @ApiModelProperty(value = "CodeGen Argument")
    public List<CodeGenTemplateArgument> getArguments() {
        return arguments;
    }

    /**
    * CodeGen Argument
    **/
    public void setArguments(List<CodeGenTemplateArgument> arguments) {
        this.arguments = arguments;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CodeGenTemplate codeGenTemplate = (CodeGenTemplate) o;
        return Objects.equals(this.name, codeGenTemplate.name) &&
        Objects.equals(this.arguments, codeGenTemplate.arguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, arguments);
    }
}

